# Debian Android Hello World Plus

This README is mostly Coffee's with some modifications by JackDca.

## What is this?

This builds on the Hello World app authored by [Coffee](https://gitlab.com/Matrixcoffee). When launched, as well as displaying a user interface, it starts a background thread which sends a message containing a time stamp every 10 seconds to the user interface, which can then displays the time stamp, for as long as the app runs. If the app is minimized and then maximized, a different message is displayed.

The Android API design treats the user interface as the most important part of an application. That approach is not always appropriate. This example can serve as a template for an app that has a minimal user interface and performs some time consuming task such as Internet access in the background.

## Why would I want this?

The Android toolchain is poorly documented; the subset of tools included in Debian Buster and what you can do with them even less so. Most documentation out there assumes you are using Android Studio or at least Gradle. These IDEs and the Android API are mind-numbingly complex. That impairs the freedom to change and add to Android systems and their components, even for experienced software developers who are developing for Android for the first time.

This project helps to document the [Android tools in Debian](https://wiki.debian.org/AndroidTools) by providing an example build. It can serve as a base template for a new app that has a simple user interface controlling some background processing, for those who would like to develop Android apps using only truly Free and Open Source software.

## Notes on the code

#### Why not use a Service for the background task?

Android provides the Service class so that the user can execute code independent of the lifecycle of the user interface components.  But, a Service runs in the main thread of the application, so is not suitable for operations that are time-consuming. For example, Android does not allow access to the Internet from the main thread. To do that, the Service would need to launch a Thread explicitly or through another class, adding a layer of complexity.

#### Subclassing Application

To provide a background thread, this example subclasses Application and overrides its onCreate() method to launch the background thread class (extended from Thread) when the app starts up.

#### Activity as a static inner class

The Application and Activity classes are both public. A source (.java) file can only contain one public class. To avoid having an additional source file, the activity class is declared as a static inner class of the application class. It must be static so that it can be instantiated independent of any instance of the application class. This allows it to be unambiguously identified as android:name=".HelloWorld$HWMainActivity" in the manifest.

The background thread class is also be an inner class of the application class. It need not be static.

This program structure conveniently allows variables and methods of the application class to be accessed from the activity and background classes. To allow access from the static activity class, the variables must also be static. This technique works because Android allows only one instance of the application class; but it should not be used to share changing data with the background because it may not be thread-safe.

(If needed, additional activity or background classes could be declared as static inner classes, but at some level of complexity, good practice would be to organize the code into more than one source file.)

#### Implementing Handler.Callback in the activity class.

Android provides Handler which allows thread-safe communication of messages from the background to the activity. The Handler is instantiated in the activity's onCreate() method. It is made available to the background via a variable of the containing application class.

Each Handler requires a Handler.Callback to receive the messages. Rather than defining a separate Handler.Callback class, the activity class implements Handler.Callback. Then the callback behaves like onStart(), onRestart(), onResume() and similar methods of the activity class.

## Status

This project results in an installable APK file, but additional testing is needed.

## How do I use this?

Assuming you are using Debian Buster, install the following packages:

```
~# apt-get install make android-sdk android-sdk-platform-23
```

This should pull in all the necessary dependencies. Let me know if you find something is missing.

Then, simply check out the project with git, and run '`make`'.

If everything goes well, you will end up with '`helloworldplus.apk`' in the root directory of the project, which you can transfer to your phone and install in any way you see fit, for example using '`adb install`'.

On the first run, you will be asked for information to create your signing key. If you're just trying things out, you can leave everything as-is.

## Credits

This builds on the Hello World app by [Coffee](https://gitlab.com/Matrixcoffee). This project would have taken much more time without the clear instructions found at [hanshq.net](https://www.hanshq.net/command-line-android.html) and to a lesser extent [Stack Overflow (via archive.org)](https://web.archive.org/web/20170222142406/http://stackoverflow.com:80/documentation/android/85/getting-started-with-android/9496/android-programming-without-an-ide#t=201709230259099678833).

## Author

New functionality added by [JackDca](https://gitlab.com/JackDca) to the original by [Coffee](https://gitlab.com/Matrixcoffee).

## Contact
* Email: brmdamon@hushmail.com

## License

This software is is licensed under the CC0 license, meaning that to the extent possible under law, the author has waived all copyright and related or neighboring rights to this work.

This software is offered as-is, and the author makes no representations or warranties of any kind concerning the software, express, implied, statutory or otherwise, including without limitation warranties of title, merchantability, fitness for a particular purpose, non infringement, or the absence of latent or other defects, accuracy, or the present or absence of errors, whether or not discoverable, all to the greatest extent permissible under applicable law.

The above is a summary; the full license can be found in the file called [LICENSE](LICENSE), or at [the creative commons website](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt).
