package net.brmdamon.helloworld;

import android.app.Application;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import java.lang.Thread;
import java.util.Date;
import java.text.SimpleDateFormat;
import android.os.Looper;
import android.os.Handler;
import android.os.Message;



public class HelloWorld extends Application {
    
    static String appData;
    
    static HWBackground background;
    
    static Handler mainHandler = null;
    
    @Override
    public void onCreate() {
        super.onCreate();
        
        appData = "Starting ...";
        
        // Create and start the background thread.
        background = new HWBackground();
        background.start();
    }


    public static class HWMainActivity extends Activity 
                                        implements Handler.Callback{
        
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            
            mainHandler = new Handler(Looper.myLooper(), this);
            
            setContentView(R.layout.activity_main);
            
            TextView textView = (TextView) findViewById(R.id.text_view);
            textView.setText("onCreate:\n"+ appData);
        }
        
        @Override
        public void onDestroy() {
            super.onDestroy();
            mainHandler = null;
        }
        
        @Override
        public void onRestart() {
            super.onRestart();

            TextView textView = (TextView) findViewById(R.id.text_view);
            textView.setText("onRestart:\n" + appData);
        }

        @Override
        public boolean handleMessage(Message msg) {
            
            TextView textView = (TextView) findViewById(R.id.text_view);
            textView.setText("handleMessage:\n" + msg.obj);
            
            return true;
        }    
    }
    
    public class HWBackground extends Thread {

        HWBackground() {
            appData = "Background - creating:\n";
        }

        @Override
        public void run() {
        
            final SimpleDateFormat timeFmt = 
                    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String wakeTime;
            
            // Do something time consuming and send results to UI.
            while(true) {
                wakeTime = timeFmt.format(new Date());
                
                if(mainHandler != null) {
                    mainHandler.sendMessage(
                            mainHandler.obtainMessage(0, wakeTime));
                } else {
                    appData = "Background - null Handler:\n" + wakeTime;
                }
                try {
                    sleep(10000);
                } catch(Exception except) {
                    appData = "Background - exception:\n" + except;
                    continue;
                }
            }
        }
    }
}
